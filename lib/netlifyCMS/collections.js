import { relative } from 'path'
import { root, cmsContent } from '/config/path'

const contentPath = relative(root, cmsContent)

/** @type {import('netlify-cms-core').CmsCollection} */
export const carousel = {
  name: 'carousel',
  label: '首頁輪播圖',
  folder: `${contentPath}/carousel`,
  slug: '{{name}}',
  identifier_field: 'name',
  fields: [
    {
      label: '名稱（可以隨便打）',
      name: 'name',
      widget: 'string',
    },
    {
      label: '圖片',
      name: 'image',
      widget: 'image',
    },
  ],
}

/** @type {import('netlify-cms-core').CmsCollection} */
export const featuredWorks = {
  name: 'featuredWorks',
  label: '首頁精選作品',
  folder: `${contentPath}/featuredWorks`,
  slug: '{{name}}',
  identifier_field: 'name',
  fields: [
    {
      label: '名稱（可以隨便打）',
      name: 'name',
      widget: 'string',
    },
    {
      label: '小圖',
      name: 'thumb',
      widget: 'image',
    },
    {
      label: '大圖',
      name: 'image',
      widget: 'image',
    },
    {
      label: '標題',
      name: 'title',
      widget: 'string',
      required: false,
    },
  ],
}

/** @type {import('netlify-cms-core').CmsCollection} */
export const works = {
  name: 'works',
  label: '作品',
  folder: `${contentPath}/works`,
  slug: '{{name}}',
  identifier_field: 'name',
  fields: [
    {
      label: '名稱（可以隨便打）',
      name: 'name',
      widget: 'string',
    },
    {
      label: '小圖',
      name: 'thumb',
      widget: 'image',
    },
    {
      label: '大圖',
      name: 'image',
      widget: 'image',
    },
    {
      label: '標題',
      name: 'title',
      widget: 'string',
      required: false,
    },
  ],
}
