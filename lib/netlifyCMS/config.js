import { relative } from 'path'
import { root, cmsAssets } from '/config/path'
import * as collections from 'lib/netlifyCMS/collections'

const assetsPath = relative(root, cmsAssets)

/** @type {import('netlify-cms-core').CmsConfig} */
export const netlifyCmsConfig = {
  load_config_file: false,
  backend: {
    name: process.env.NEXT_PUBLIC_NETLIFY_CMS_BACKEND_NAME,
    repo: process.env.NEXT_PUBLIC_NETLIFY_CMS_BACKEND_REPO,
    branch: process.env.NEXT_PUBLIC_NETLIFY_CMS_BACKEND_BRANCH,
    app_id: process.env.NEXT_PUBLIC_NETLIFY_CMS_BACKEND_APP_ID,
    auth_type: process.env.NEXT_PUBLIC_NETLIFY_CMS_BACKEND_AUTH_TYPE,
  },
  local_backend: {
    url: '/api/v1',
  },
  display_url: '/',
  locale: 'zh_Hant',
  media_folder: assetsPath,
  collections: Object.values(collections).map(collection => ({
    create: true,
    format: 'json',
    ...collection,
  })),
}

export default netlifyCmsConfig
