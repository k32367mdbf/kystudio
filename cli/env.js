import defaultEnv, { expose as defaultExpose } from '/config/env.default'

const { env } = process

const {
  expose: targetExpose = {},
  ...targetEnv
} = await import(`/config/env.${env.NODE_ENV}`).catch(() => ({}))

const {
  expose: customExpose = {},
  ...customEnv
} = await import('/config/env').catch(() => ({}))

export const get = () => ({
  ...defaultEnv, ...targetEnv, ...customEnv,
  expose: { ...defaultExpose, ...targetExpose, ...customExpose },
})

export const set = () => {
  // compose environment variables that using in backend
  const envVariables = { ...defaultEnv, ...targetEnv.default, ...customEnv.default }
  Object.entries(envVariables).forEach(([key, value]) => {
    // respect for system and inline environment variables
    env[key] ?? (env[key] = value)
  })

  // compose environment variables that using in frontend
  const exposeEnv = { ...defaultExpose, ...targetExpose, ...customExpose }
  Object.entries(exposeEnv).forEach(([key, value]) => {
    // respect for system and inline environment variables
    env[`NEXT_PUBLIC_${key}`] ?? (env[`NEXT_PUBLIC_${key}`] = value)
  })
}

export default get
