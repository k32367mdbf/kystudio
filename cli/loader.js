export const resolve = async (specifier, context, defaultResolve) => defaultResolve(
  specifier.startsWith('/')
    ? `file://${process.cwd()}${specifier}${specifier.includes('.js') || specifier.includes('.cjs') ? '' : '.js'}`
    : specifier,
  context,
  defaultResolve,
)
