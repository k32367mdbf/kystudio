/* eslint-disable no-console */

import { createServer } from 'http'
import { spawn } from 'child_process'

import next from 'next'
import nextBuild from 'next/dist/build/index.js'
import nextGenerate from 'next/dist/export/index.js'
import nextLoadConfig from 'next/dist/server/config.js'
import { trace } from 'next/dist/trace/trace.js'

import nextConfig from '/config/next'
import * as dir from '/config/path'
import { set as setEnv } from '/cli/env'

const build = nextBuild.default
const generate = nextGenerate.default
const loadConfig = nextLoadConfig.default

const cmd = process.argv[2]
const opt = { stdio: 'inherit', shell: true }
const { env } = process

// setup environment variables
setEnv()

'start'
== cmd && new Promise(async () => {
  const app = next({ dev: false, conf: nextConfig })
  await app.prepare()
  await new Promise(ok => createServer(app.getRequestHandler()).listen(env.PORT, ok))
  console.log('\x1b[33m%s\x1b[0m', `Listening on: http://localhost:${env.PORT}`)
})

'develop'
== cmd && new Promise(async () => {
  const express = (await import('express')).default
  const netlifyServer = (await import('netlify-cms-proxy-server/dist/middlewares.js')).default
  const { registerLocalFs } = netlifyServer
  const server = express()
  const app = next({ dev: true, conf: nextConfig })
  const handle = app.getRequestHandler()
  server.get('*', handle)
  await app.prepare()
  await registerLocalFs(server)
  await new Promise(ok => server.listen(env.PORT, ok))
  console.log('\x1b[33m%s\x1b[0m', `Listening on: http://localhost:${env.PORT}`)
})

'build'
== cmd && new Promise(async () => {
  await build(dir.root, nextConfig)
  const defaultConfig = await loadConfig('phase-export', dir.root)
  await generate(dir.root, { outdir: dir.dist }, trace(), { ...defaultConfig, ...nextConfig })
  console.log('\x1b[33m%s\x1b[0m', `Successfully generated in ${dir.dist}`)
  process.exit()
})

'lint'
== cmd && new Promise(async () => {
  const lint = spawn(`eslint ${dir.root}`, [
    '--cache',
    '--cache-location', `${dir.cache}/eslint/`,
    '--max-warnings', 0,
  ], opt)
  lint.on('close', code => process.exit(code))
})

// enable input reading
process.stdin.on('data', () => {})
