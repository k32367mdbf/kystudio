import { useRouter } from 'next/router'
import Link from 'next/link'

export const Header = () => {
  const route = useRouter().pathname

  return (
    <header className="site-navbar site-navbar-target bg-white" role="banner">
      <div className="container">
        <div className="row align-items-center position-relative">
          <div className="col-lg-4">
            <nav className="site-navigation text-right ml-auto " role="navigation">
              <ul className="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block">
                <li className={route === '/' ? 'active' : ''}>
                  <a href='/' className="nav-link">首頁</a>
                </li>
                <li className={route === '/work' ? 'active' : ''}>
                  <Link href='/work' passHref><a className="nav-link">作品</a></Link>
                </li>
                {/* <li className={route === '/service' ? 'active' : ''}>
                  <Link href='/service' passHref><a className="nav-link">服務</a></Link>
                </li> */}
              </ul>
            </nav>
          </div>
          <div className="col-lg-4 text-center">
            <a href='/'>
              <div className="site-logo col-lg-12">
                宗橋室內裝修
              </div>
              <div className="site-logo col-lg-12 text-">
                ✕
              </div>
              <div className="col-lg-12">
                快意空間
              </div>
            </a>
            <div className="ml-auto toggle-button d-inline-block d-lg-none">
              <a href="#" className="site-menu-toggle py-5 js-menu-toggle text-white">
                <span className="icon-menu h3 text-primary"></span>
              </a>
            </div>
          </div>
          <div className="col-lg-4">
            <nav className="site-navigation text-left mr-auto " role="navigation">
              <ul className="site-menu main-menu js-clone-nav ml-auto d-none d-lg-block">
                {/* <li className={route === '/about' ? 'active' : ''}>
                  <Link href='/about' passHref><a className="nav-link">關於</a></Link>
                </li> */}
                {/* <li className={route === '/gallery' ? 'active' : ''}>
                  <Link href='/gallery' passHref><a className="nav-link">畫冊</a></Link>
                </li> */}
                <li className={route === '/contact' ? 'active' : ''}>
                  <Link href='/contact' passHref><a className="nav-link">聯絡我們</a></Link>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </div>
    </header>
  )
}

export default Header
