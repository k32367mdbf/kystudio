import { useEffect } from 'react'
import Header from 'parts/Header'
import Footer from 'parts/Footer'

export const DefaultLayout = ({ children }) => {
  useEffect(() => {
    import('public/js/main')
  }, [])

  return (
    <div className="site-wrap" id="home-section">

      <div className="site-mobile-menu site-navbar-target">
        <div className="site-mobile-menu-header">
          <div className="site-mobile-menu-close mt-3">
            <span className="icon-close2 js-menu-toggle"></span>
          </div>
        </div>
        <div className="site-mobile-menu-body"></div>
      </div>

      <Header />
      {children}
      <Footer />
    </div>
  )
}

export default DefaultLayout
