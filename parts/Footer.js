export const Footer = () => (
  <footer className="site-footer bg-light">
    <div className="container">
      <div className="row">
        <div className="col-md-8">
          <div className="row">
            <div className="col-md-7">
              <h2 className="footer-heading mb-4">關於我們</h2>
              <p>營建署建築物室內裝修專業技術人員</p>
              <p>內營室技字第40EB022367號</p>
              <p>以及專業的團隊</p>
            </div>
            {/* <div className="col-md-4 ml-auto">
              <h2 className="footer-heading mb-4">更多資訊</h2>
              <ul className="list-unstyled">
                <li><a href="#">關於我們</a></li>
                <li><a href="#">合作夥伴</a></li>
                <li><a href="#">服務條款</a></li>
                <li><a href="#">隱私權政策</a></li>
                <li><a href="#">聯絡我們</a></li>
              </ul>
            </div> */}

          </div>
        </div>
        <div className="col-md-4 ml-auto">
          {/* <div className="mb-5">
            <h2 className="footer-heading mb-4">訂閱我們的最新資訊</h2>
            <form action="#" method="post" className="footer-suscribe-form">
              <div className="input-group mb-3">
                <input type="text"
                  className="form-control rounded-0 border-secondary text-white bg-transparent"
                  placeholder="Enter Email" aria-label="Enter Email"
                  aria-describedby="button-addon2" />
                <div className="input-group-append">
                  <button className="btn btn-primary text-white" type="button"
                    id="button-addon2">訂閱</button>
                </div>
              </div>
            </form>
          </div> */}
          <h2 className="footer-heading mb-4">追蹤我們</h2>
          <a href="https://www.facebook.com/100057041635748" target="_blank" className="pl-0 pr-3">
            <span className="icon-facebook" />
          </a>
          <a href="https://www.youtube.com/channel/UCfHEsXYVsmBh8DLM9Qg3UZA" target="_blank" className="pl-3 pr-3">
            <span className="icon-youtube-play" />
          </a>
        </div>
      </div>
      <div className="row pt-5 mt-5 text-center">
        <div className="col-md-12">
          <div className="pt-5">
            <p>
              {/* Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. */}
              {/* Copyright &copy;
              <script>document.write(new Date().getFullYear());</script> All rights reserved | This
              template is made
              with <i className="icon-heart text-danger" aria-hidden="true"></i> by <a
                href="https://colorlib.com" target="_blank">Colorlib</a> */}
              {/* Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. */}
            </p>
          </div>
        </div>
      </div>
    </div>
  </footer>
)

export default Footer
