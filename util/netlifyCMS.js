import { readdirSync as readDir, readFileSync as read } from 'fs'

/**
 * load netlify cms data in specific folder or files
 * @param {import('netlify-cms-core').CmsCollection} collection netlify cms collection
 * @returns {unknown|unknown[]} data
 */
export const loadData = collection => {
  try {
    if (collection?.folder) {
      const folderPath = collection.folder
      const files = readDir(folderPath).map(file => `${folderPath}/${file}`)
      const data = files.map(file => JSON.parse(read(file)))

      return data
    } else if (collection?.files) {
      const data = {}
      collection.files.forEach(({ name, file }) => {
        data[name] = JSON.parse(read(file))
      })

      return data
    }
  } catch (error) {
    return []
  }
}

export default loadData
