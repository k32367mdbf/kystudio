import dynamic from 'next/dynamic'
import Head from 'next/head'
import DefaultLayout from 'parts/layout/Default'

const App = ({ Component, pageProps }) => {
  const title = [Component?.title, '宗橋室內裝修']
    .filter(v => v)
    .join(' | ')
  const Layout = Component.Layout || DefaultLayout

  return (
    <>
      <Head>
        <title children={title} />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
      </Head>

      <Layout>
        <Component {...pageProps} />
      </Layout>
    </>
  )
}

// Disable SSR globally
export default dynamic(() => Promise.resolve(App), { ssr: false })
