import { useEffect } from 'react'
import Link from 'next/link'
import { nextConfig } from '/config/next'
import { works } from 'lib/netlifyCMS/collections'
import { loadData } from 'util/netlifyCMS'

const { basePath = '' } = nextConfig

export function getStaticProps() {
  const data = loadData(works).map(work => ({
    ...work,
    thumb: basePath + work.thumb.split('/public')[1],
    image: work.image.split('/public')[1],
  }))

  return {
    props: { works: data },
  }
}

export const Works = ({ works }) => {
  useEffect(() => {
    // 作品集動畫
    /* global AnimOnScroll */
    new AnimOnScroll(document.querySelector('#grid'), {
      minDuration: 0.4,
      maxDuration: 0.7,
      viewportFactor: 0.2,
    })
  }, [])

  return (
    <>
      <div className="site-section">
        <div className="container">

          <div className="row mb-5 align-items-center">
            <div className="col-md-7">
              <h2 className="heading-39291 mb-0">作品</h2>
            </div>
          </div>

          <ul className="grid col-50 effect-5" id="grid">
            {works.length ? works.map((work, i) => (
              <li className="media-02819" key={i}>
                <Link href={work.image} passHref>
                  <a target="_blank" className="img-link"><img src={work.thumb} alt="Image" className="img-fluid" /></a>
                </Link>
                {/* <h3>
                  <Link href={work.image || '#'} passHref>
                    <a>{work.title}</a>
                  </Link>
                </h3>
                <span>{work.name}</span> */}
              </li>
            )) : <li></li>}
          </ul>

          <p className="mb-0 text-center">
            <a href="https://www.facebook.com/100057041635748" target="_blank" className="more-39291">
              歡迎到我們的粉絲專頁看看更多作品
            </a>
          </p>

        </div>
      </div>

      {/* <div className="site-section section-4">
        <div className="container">

          <div className="row justify-content-center text-center">
            <div className="col-md-7">
              <div className="slide-one-item owl-carousel">
                <blockquote className="testimonial-1">
                  <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus totam sit delectus
                    earum facere ex ea sunt, eos?</p>
                  <cite><span className="text-black">Mike Dorney</span> &mdash; <span className="text-muted">CEO
                    and Co-Founder</span></cite>
                </blockquote>

                <blockquote className="testimonial-1">
                  <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                  <p>Eligendi earum ad perferendis dolores, dolor quas. Ullam in, eaque mollitia suscipit
                    id aspernatur rerum! Sit quibusdam ullam tempora quis, in voluptatum maiores
                    veritatis recusandae!</p>
                  <cite><span className="text-black">James Smith</span> &mdash; <span className="text-muted">CEO
                    and Co-Founder</span></cite>
                </blockquote>

                <blockquote className="testimonial-1">
                  <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                  <p> Officia, eius omnis rem non quis eos excepturi cumque sequi pariatur eaque quasi
                    nihil dicta tempore voluptate culpa, veritatis incidunt voluptatibus qui?</p>
                  <cite><span className="text-black">Mike Dorney</span> &mdash; <span className="text-muted">CEO
                    and Co-Founder</span></cite>
                </blockquote>
              </div>
            </div>
          </div>
        </div>
      </div> */}
    </>
  )
}

Works.title = '作品'

export default Works
