// import { useRouter } from 'next/router'
import Link from 'next/link'

const work = {}

export const Work = () => {
  // const router = useRouter()
  // const { id } = router.query

  return (
    <div className="site-section">
      <div className="container">
        <div className="media-02819">
          <h3>
            <Link href={work.postLink || ''} passHref>
              <a>{work.title}</a>
            </Link>
          </h3>
          <span>{work.client}</span>
        </div>
        <ul className="grid effect-5" id="grid">
          <li className="media-02819">
            <a href={work.photo} target="_blank"><img src={work.photo} /></a>
          </li>
        </ul>
      </div>
    </div>
  )
}

export default Work
