export const Contact = () => (
  <>
    <div className="site-section">
      <div className="container">

        <div className="row">
          {/* <div className="col-lg-8 mb-5">
            <form action="#" method="post">
              <div className="form-group row">
                <div className="col-md-6 mb-4 mb-lg-0">
                  <input type="text" className="form-control" placeholder="First name" />
                </div>
                <div className="col-md-6">
                  <input type="text" className="form-control" placeholder="First name" />
                </div>
              </div>

              <div className="form-group row">
                <div className="col-md-12">
                  <input type="text" className="form-control" placeholder="Email address" />
                </div>
              </div>

              <div className="form-group row">
                <div className="col-md-12">
                  <textarea name="" id="" className="form-control" placeholder="Write your message." cols="30"
                    rows="10"></textarea>
                </div>
              </div>
              <div className="form-group row">
                <div className="col-md-6 mr-auto">
                  <input type="submit" className="btn btn-block btn-primary text-white py-3 px-5 rounded-0"
                    value="Send Message" />
                </div>
              </div>
            </form>
          </div> */}

          <div className="col-lg-6 ml-auto">
            <div className="bg-white p-3 p-md-5">
              <h3 className="heading-39291">聯絡資訊</h3>
              <ul className="list-unstyled footer-link">
                <h5 className="heading-39291">郭先生</h5>
                <li className="d-block mb-3 font-weight-bold">
                  <h5 className="font-weight-bold">營建署建築物室內裝修專業技術人員</h5>
                  <h5 className="font-weight-bold">內營室技字第40EB022367號</h5>
                </li>
                <li className="d-block mb-3">
                  <span className="d-block text-black small text-uppercase font-weight-bold">電話:</span>
                  <span className="icon-phone mr-1" />
                  <a href="tel:0955077880">0955077880</a>
                </li>
                <li className="d-block mb-3">
                  <span className="d-block text-black small text-uppercase font-weight-bold">歡迎來電詢價、比價</span>
                </li>
              </ul>
            </div>
          </div>

          <div className="col-lg-6 ml-auto" data-aos="fade-up" data-aos-delay="100">
            <a href="https://www.facebook.com/100057041635748" target="_blank">
              <div className="service-29193 text-center">
                <span className="icon-facebook" style={{ color: 'blue', fontSize: '5rem' }} />
                <h3 className="my-4">宗橋室內裝修工程</h3>
                {/* <p>Lorem ipsum dolor sit ame adipisicing elit.</p> */}
              </div>
            </a>
          </div>

        </div>
      </div>
    </div>

    {/* <div className="site-section section-4">
      <div className="container">

        <div className="row justify-content-center text-center">
          <div className="col-md-7">
            <div className="slide-one-item owl-carousel">
              <blockquote className="testimonial-1">
                <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus totam sit delectus
                  earum facere ex ea sunt, eos?</p>
                <cite><span className="text-black">Mike Dorney</span> &mdash; <span className="text-muted">CEO
                  and Co-Founder</span></cite>
              </blockquote>

              <blockquote className="testimonial-1">
                <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                <p>Eligendi earum ad perferendis dolores, dolor quas. Ullam in, eaque mollitia suscipit
                  id aspernatur rerum! Sit quibusdam ullam tempora quis, in voluptatum maiores
                  veritatis recusandae!</p>
                <cite><span className="text-black">James Smith</span> &mdash; <span className="text-muted">CEO
                  and Co-Founder</span></cite>
              </blockquote>

              <blockquote className="testimonial-1">
                <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                <p> Officia, eius omnis rem non quis eos excepturi cumque sequi pariatur eaque quasi
                  nihil dicta tempore voluptate culpa, veritatis incidunt voluptatibus qui?</p>
                <cite><span className="text-black">Mike Dorney</span> &mdash; <span className="text-muted">CEO
                  and Co-Founder</span></cite>
              </blockquote>
            </div>
          </div>
        </div>
      </div>
    </div> */}
  </>
)

Contact.title = '聯絡我們'

export default Contact
