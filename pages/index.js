import { useEffect } from 'react'
import Link from 'next/link'
import { nextConfig } from '/config/next'
import { carousel, featuredWorks } from 'lib/netlifyCMS/collections'
import { loadData } from 'util/netlifyCMS'

const { basePath = '' } = nextConfig

export function getStaticProps() {
  const carouselData = loadData(carousel).map(content => ({
    ...content,
    image: basePath + content.image.split('/public')[1],
  }))

  const featuredWorksData = loadData(featuredWorks).map(work => ({
    ...work,
    thumb: basePath + work.thumb.split('/public')[1],
    image: work.image.split('/public')[1],
  }))

  return {
    props: { carouselData, featuredWorks: featuredWorksData },
  }
}

export const Home = ({
  carouselData,
  featuredWorks,
}) => {
  useEffect(() => {
    // 作品集動畫
    /* global AnimOnScroll */
    new AnimOnScroll(document.querySelector('#grid'), {
      minDuration : 0.4,
      maxDuration : 0.7,
      viewportFactor : 0.2,
    })
  }, [])

  return (
    <>
      <div className="owl-carousel-wrapper">
        <div className="box-92819 shadow-lg">
          <div>
            <h2 className="mb-3 text-white">專業室內裝修</h2>
            <h2 className="mb-3 text-white">專業舊屋整修</h2>
            <h2 className="mb-3 text-white">專業鐵屋廠房</h2>
            <p>用專業的服務、實在的價格，用心為您打造最舒適的環境</p>
            <p className="mb-0 mt-4">
              <Link href='/contact' passHref>
                <a className="btn btn-primary">歡迎來電詢價、比價</a>
              </Link>
              <li className="d-block mb-3 text-white mt-1">
                <span className="icon-phone mr-2" />
                <a href="tel:0955077880" className="text-white">0955077880</a>
              </li>
            </p>
          </div>
        </div>

        <div className="owl-carousel owl-1 ">
          {carouselData.map((data, i) => (
            <div className="ftco-cover-1" key={i} style={{ backgroundImage: `url("${data.image}")` }} />
          ))}
        </div>
      </div>

      <div className="site-section">
        <div className="container">

          <div className="row mb-5 align-items-center">
            <div className="col-md-7">
              <h2 className="heading-39291 mb-0">作品</h2>
            </div>
            <div className="col-md-5 text-right">
              <p className="mb-0">
                <a href="/work" className="more-39291">瀏覽所有作品</a>
              </p>
            </div>
          </div>

          <ul className="grid col-50 effect-5" id="grid">
            {featuredWorks.length ? featuredWorks.map((work, i) => (
              <li className="media-02819" key={i}>
                <Link href={work.image} passHref>
                  <a target="_blank" className="img-link">
                    <img src={work.thumb} alt="Image" className="img-fluid" />
                  </a>
                </Link>
                {/* <h3>
                  <Link href={work.postLink || '#'} passHref>
                    <a>{work.title}</a>
                  </Link>
                </h3>
                <span>{work.name}</span> */}
              </li>
            )) : <li></li>}
          </ul>

          <p className="mb-0 text-center">
            <a href="https://www.facebook.com/100057041635748" target="_blank" className="more-39291">
              歡迎到我們的粉絲專頁看看更多作品
            </a>
          </p>
        </div>
      </div>

      <div className="site-section bg-primary">
        <div className="container">
          <div className="row mb-5 align-items-center">
            <div className="col-md-7">
              <h2 className="heading-39291 text-white mb-3">追蹤我們</h2>
              {/* <p className="text-white">我們提供各種專業服務</p> */}
            </div>
          </div>
          <div className="row">

            <div className="col-md-6 mb-4 col-lg-4" data-aos="fade-up" data-aos-delay="100">
              <a href="https://www.facebook.com/1207129352714794" target="_blank">
                <div className="service-29193 text-center">
                  <span className="icon-facebook" style={{ color: 'blue', fontSize: '5rem' }} />
                  <h3 className="my-4">宗來工作室</h3>
                  {/* <p>Lorem ipsum dolor sit ame adipisicing elit.</p> */}
                </div>
              </a>
            </div>

            <div className="col-md-6 mb-4 col-lg-4" data-aos="fade-up" data-aos-delay="100">
              <a href="https://www.facebook.com/100057041635748" target="_blank">
                <div className="service-29193 text-center">
                  <span className="icon-facebook" style={{ color: 'blue', fontSize: '5rem' }} />
                  <h3 className="my-4">宗橋室內裝修工程</h3>
                  {/* <p>Lorem ipsum dolor sit ame adipisicing elit.</p> */}
                </div>
              </a>
            </div>

            <div className="col-md-6 mb-4 col-lg-4" data-aos="fade-up" data-aos-delay="100">
              <a href="https://www.youtube.com/channel/UCfHEsXYVsmBh8DLM9Qg3UZA" target="_blank">
                <div className="service-29193 text-center">
                  <span className="icon-youtube-play" style={{ color: 'red', fontSize: '5rem' }} />
                  <h3 className="my-4">快意工坊</h3>
                  {/* <p>Lorem ipsum dolor sit ame adipisicing elit.</p> */}
                </div>
              </a>
            </div>

          </div>
        </div>
      </div>

      {/* <div className="site-section section-4">
        <div className="container">

          <div className="row justify-content-center text-center">
            <div className="col-md-7">
              <div className="slide-one-item owl-carousel">
                <blockquote className="testimonial-1">
                  <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus totam sit delectus
                    earum facere ex
                    ea sunt, eos?</p>
                  <cite><span className="text-black">Mike Dorney</span> &mdash; <span className="text-muted">CEO
                    and
                    Co-Founder</span></cite>
                </blockquote>

                <blockquote className="testimonial-1">
                  <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                  <p>Eligendi earum ad perferendis dolores, dolor quas. Ullam in, eaque mollitia suscipit
                    id aspernatur
                    rerum! Sit quibusdam ullam tempora quis, in voluptatum maiores veritatis recusandae!
                  </p>
                  <cite><span className="text-black">James Smith</span> &mdash; <span className="text-muted">CEO
                    and
                    Co-Founder</span></cite>
                </blockquote>

                <blockquote className="testimonial-1">
                  <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                  <p> Officia, eius omnis rem non quis eos excepturi cumque sequi pariatur eaque quasi
                    nihil dicta tempore
                    voluptate culpa, veritatis incidunt voluptatibus qui?</p>
                  <cite><span className="text-black">Mike Dorney</span> &mdash; <span className="text-muted">CEO
                    and
                    Co-Founder</span></cite>
                </blockquote>
              </div>
            </div>
          </div>
        </div>
      </div> */}
    </>
  )
}

export default Home
