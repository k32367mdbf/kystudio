import Link from 'next/link'

const works = []

export const Gallery = () => (
  <>
    <div className="container">
      <ul className="grid effect-5" id="grid">
        {works.map((work, i) => (
          <li className="media-02819" key={i}>
            <Link href={work.postLink} passHref>
              <a><img src={work.photo} /></a>
            </Link>
          </li>
        ))}
      </ul>
    </div>

    <div className="site-section">
      <div className="container">
        <div className="row">

          <div className="col-lg-12">
            <div className="owl-carousel owl-3">
              <a href="/images/hero_1.jpg" className="" data-fancybox="gal"><img src="/images/hero_1.jpg" alt="Image"
                className="img-fluid" /></a>
              <a href="/images/hero_2.jpg" className="" data-fancybox="gal"><img src="/images/hero_2.jpg" alt="Image"
                className="img-fluid" /></a>
              <a href="/images/hero_3.jpg" className="" data-fancybox="gal"><img src="/images/hero_3.jpg" alt="Image"
                className="img-fluid" /></a>
              <a href="/images/hero_1.jpg" className="" data-fancybox="gal"><img src="/images/hero_1.jpg" alt="Image"
                className="img-fluid" /></a>
              <a href="/images/hero_2.jpg" className="" data-fancybox="gal"><img src="/images/hero_2.jpg" alt="Image"
                className="img-fluid" /></a>
              <a href="/images/hero_3.jpg" className="" data-fancybox="gal"><img src="/images/hero_3.jpg" alt="Image"
                className="img-fluid" /></a>
              <a href="/images/hero_1.jpg" className="" data-fancybox="gal"><img src="/images/hero_1.jpg" alt="Image"
                className="img-fluid" /></a>
              <a href="/images/hero_2.jpg" className="" data-fancybox="gal"><img src="/images/hero_2.jpg" alt="Image"
                className="img-fluid" /></a>
              <a href="/images/hero_3.jpg" className="" data-fancybox="gal"><img src="/images/hero_3.jpg" alt="Image"
                className="img-fluid" /></a>
            </div>

          </div>
        </div>
      </div>
    </div>

    <div className="site-section section-4">
      <div className="container">

        <div className="row justify-content-center text-center">
          <div className="col-md-7">
            <div className="slide-one-item owl-carousel">
              <blockquote className="testimonial-1">
                <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus totam sit delectus
                  earum facere ex ea sunt, eos?</p>
                <cite><span className="text-black">Mike Dorney</span> &mdash; <span className="text-muted">CEO
                  and Co-Founder</span></cite>
              </blockquote>

              <blockquote className="testimonial-1">
                <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                <p>Eligendi earum ad perferendis dolores, dolor quas. Ullam in, eaque mollitia suscipit
                  id aspernatur rerum! Sit quibusdam ullam tempora quis, in voluptatum maiores
                  veritatis recusandae!</p>
                <cite><span className="text-black">James Smith</span> &mdash; <span className="text-muted">CEO
                  and Co-Founder</span></cite>
              </blockquote>

              <blockquote className="testimonial-1">
                <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                <p> Officia, eius omnis rem non quis eos excepturi cumque sequi pariatur eaque quasi
                  nihil dicta tempore voluptate culpa, veritatis incidunt voluptatibus qui?</p>
                <cite><span className="text-black">Mike Dorney</span> &mdash; <span className="text-muted">CEO
                  and Co-Founder</span></cite>
              </blockquote>
            </div>
          </div>
        </div>
      </div>
    </div>
  </>
)

export default Gallery
