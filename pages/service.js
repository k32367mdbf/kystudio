export const Service = () => (
  <>
    <div className="site-section bg-light">
      <div className="container">
        <div className="row mb-5 align-items-center">
          <div className="col-md-7">
            <h2 className="heading-39291 text-black mb-3">Services</h2>
            <p className="text-black">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis velit
              iure possimus repellendus, esse minus illum nobis deleniti?</p>
          </div>
        </div>
        <div className="row">

          <div className="col-md-6 mb-4 col-lg-4" data-aos="fade-up" data-aos-delay="100">
            <div className="service-29193 text-center">
              <span className="img-wrap mb-5">
                <img src="fonts/flaticon/svg/002-kitchen.svg" alt="Image" className="img-fluid" />
              </span>
              <h3 className="mb-4"><a href="#">Reiciendis Velit</a></h3>
              <p>Lorem ipsum dolor sit ame adipisicing elit. Perspiciatis incidunt distinctio voluptate .
              </p>
            </div>
          </div>
          <div className="col-md-6 mb-4 col-lg-4" data-aos="fade-up" data-aos-delay="200">
            <div className="service-29193 text-center">
              <span className="img-wrap mb-5">
                <img src="fonts/flaticon/svg/003-lamp.svg" alt="Image" className="img-fluid" />
              </span>
              <h3 className="mb-4"><a href="#">Incidunt Distinctio</a></h3>
              <p>Lorem ipsum dolor sit ame adipisicing elit. Perspiciatis incidunt distinctio voluptate .
              </p>
            </div>
          </div>

          <div className="col-md-6 mb-4 col-lg-4" data-aos="fade-up" data-aos-delay="">
            <div className="service-29193 text-center">
              <span className="img-wrap mb-5">
                <img src="fonts/flaticon/svg/001-stairs.svg" alt="Image" className="img-fluid" />
              </span>
              <h3 className="mb-4"><a href="#">Reiciendis Velit Iure</a></h3>
              <p>Lorem ipsum dolor sit ame adipisicing elit. Perspiciatis incidunt distinctio voluptate .
              </p>
            </div>
          </div>

          <div className="col-md-6 mb-4 col-lg-4" data-aos="fade-up" data-aos-delay="">
            <div className="service-29193 text-center">
              <span className="img-wrap mb-5">
                <img src="fonts/flaticon/svg/004-blueprint.svg" alt="Image" className="img-fluid" />
              </span>
              <h3 className="mb-4"><a href="#">Boluptate Ipsum</a></h3>
              <p>Lorem ipsum dolor sit ame adipisicing elit. Perspiciatis incidunt distinctio voluptate .
              </p>
            </div>
          </div>
          <div className="col-md-6 mb-4 col-lg-4" data-aos="fade-up" data-aos-delay="200">
            <div className="service-29193 text-center">
              <span className="img-wrap mb-5">
                <img src="fonts/flaticon/svg/006-pantone.svg" alt="Image" className="img-fluid" />
              </span>
              <h3 className="mb-4"><a href="#">Modern Elit</a></h3>
              <p>Lorem ipsum dolor sit ame adipisicing elit. Perspiciatis incidunt distinctio voluptate .
              </p>
            </div>
          </div>
          <div className="col-md-6 mb-4 col-lg-4" data-aos="fade-up" data-aos-delay="100">
            <div className="service-29193 text-center">
              <span className="img-wrap mb-5">
                <img src="fonts/flaticon/svg/005-dinning-table.svg" alt="Image" className="img-fluid" />
              </span>
              <h3 className="mb-4"><a href="#">Dolor Sitame</a></h3>
              <p>Lorem ipsum dolor sit ame adipisicing elit. Perspiciatis incidunt distinctio voluptate .
              </p>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div className="site-section section-4">
      <div className="container">

        <div className="row justify-content-center text-center">
          <div className="col-md-7">
            <div className="slide-one-item owl-carousel">
              <blockquote className="testimonial-1">
                <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus totam sit delectus
                  earum facere ex ea sunt, eos?</p>
                <cite><span className="text-black">Mike Dorney</span> &mdash; <span className="text-muted">CEO
                  and Co-Founder</span></cite>
              </blockquote>

              <blockquote className="testimonial-1">
                <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                <p>Eligendi earum ad perferendis dolores, dolor quas. Ullam in, eaque mollitia suscipit
                  id aspernatur rerum! Sit quibusdam ullam tempora quis, in voluptatum maiores
                  veritatis recusandae!</p>
                <cite><span className="text-black">James Smith</span> &mdash; <span className="text-muted">CEO
                  and Co-Founder</span></cite>
              </blockquote>

              <blockquote className="testimonial-1">
                <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                <p> Officia, eius omnis rem non quis eos excepturi cumque sequi pariatur eaque quasi
                  nihil dicta tempore voluptate culpa, veritatis incidunt voluptatibus qui?</p>
                <cite><span className="text-black">Mike Dorney</span> &mdash; <span className="text-muted">CEO
                  and Co-Founder</span></cite>
              </blockquote>
            </div>
          </div>
        </div>
      </div>
    </div>
  </>
)

export default Service
