const teams = []

export const About = () => (
  <>
    <div className="site-section">
      <div className="container">
        <div className="row align-items-stretch">
          <div className="col-lg-4">
            <div className="h-100 bg-white box-29291">
              <h2 className="heading-39291">Company History</h2>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo mollitia id ea ab in! Nam
                eligendi distinctio, vitae.</p>
              <p>Alias odit ipsam quas unde obcaecati molestiae consequatur numquam cupiditate perferendis
                facere, nulla nemo id, accusantium corrupti tempora.</p>

            </div>
          </div>
          <div className="col-lg-8">
            <div className="owl-carousel owl-3">
              <img src="/images/about_1.jpg" alt="Image" className="img-fluid" />
              <img src="/images/about_2.jpg" alt="Image" className="img-fluid" />
              <img src="/images/about_3.jpg" alt="Image" className="img-fluid" />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div className="site-section bg-light">
      <div className="container">
        <div className="row justify-content-center text-center mb-5 section-2-title">
          <div className="col-md-6">
            <h2 className="heading-39291">Meet Our Team</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis provident eius ratione
              velit, voluptas laborum nemo quas ad necessitatibus placeat?</p>
          </div>
        </div>
        <div className="row align-items-stretch">

          {teams.map((team, i) => (
            <div className="col-lg-4 col-md-6 mb-5" key={i}>
              <div className="post-entry-1 h-100 bg-white text-center">
                <a href="#" className="d-inline-block">
                  <img src={team.photo} alt="Image" className="img-fluid" />
                </a>
                <div className="post-entry-1-contents">
                  <span className="meta">{team.position}</span>
                  <h2>{team.memberName}</h2>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, sapiente.</p>
                </div>
              </div>
            </div>
          ))}

          <div className="col-lg-4 col-md-6 mb-5">
            <div className="post-entry-1 h-100 bg-white text-center">
              <a href="#" className="d-inline-block">
                <img src="/images/person_2.jpg" alt="Image" className="img-fluid" />
              </a>
              <div className="post-entry-1-contents">
                <span className="meta">Founder</span>
                <h2>James Doe</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, sapiente.</p>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 mb-5">
            <div className="post-entry-1 h-100 bg-white text-center">
              <a href="#" className="d-inline-block">
                <img src="/images/person_3.jpg" alt="Image" className="img-fluid" />
              </a>
              <div className="post-entry-1-contents">
                <span className="meta">Founder</span>
                <h2>James Doe</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, sapiente.</p>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 mb-5">
            <div className="post-entry-1 h-100 bg-white text-center">
              <a href="#" className="d-inline-block">
                <img src="/images/person_4.jpg" alt="Image" className="img-fluid" />
              </a>
              <div className="post-entry-1-contents">
                <span className="meta">Founder</span>
                <h2>James Doe</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, sapiente.</p>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 mb-5">
            <div className="post-entry-1 h-100 bg-white text-center">
              <a href="#" className="d-inline-block">
                <img src="/images/person_5.jpg" alt="Image" className="img-fluid" />
              </a>
              <div className="post-entry-1-contents">
                <span className="meta">Founder</span>
                <h2>James Doe</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, sapiente.</p>
              </div>
            </div>
          </div>

          <div className="col-lg-4 col-md-6 mb-5">
            <div className="post-entry-1 h-100 bg-white text-center">
              <a href="#" className="d-inline-block">
                <img src="/images/person_1.jpg" alt="Image" className="img-fluid" />
              </a>
              <div className="post-entry-1-contents">
                <span className="meta">Founder</span>
                <h2>James Doe</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, sapiente.</p>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div className="site-section section-4">
      <div className="container">

        <div className="row justify-content-center text-center">
          <div className="col-md-7">
            <div className="slide-one-item owl-carousel">
              <blockquote className="testimonial-1">
                <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus totam sit delectus
                  earum facere ex ea sunt, eos?</p>
                <cite><span className="text-black">Mike Dorney</span> &mdash; <span className="text-muted">CEO
                  and Co-Founder</span></cite>
              </blockquote>

              <blockquote className="testimonial-1">
                <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                <p>Eligendi earum ad perferendis dolores, dolor quas. Ullam in, eaque mollitia suscipit
                  id aspernatur rerum! Sit quibusdam ullam tempora quis, in voluptatum maiores
                  veritatis recusandae!</p>
                <cite><span className="text-black">James Smith</span> &mdash; <span className="text-muted">CEO
                  and Co-Founder</span></cite>
              </blockquote>

              <blockquote className="testimonial-1">
                <span className="quote quote-icon-wrap"><span className="icon-format_quote"></span></span>
                <p> Officia, eius omnis rem non quis eos excepturi cumque sequi pariatur eaque quasi
                  nihil dicta tempore voluptate culpa, veritatis incidunt voluptatibus qui?</p>
                <cite><span className="text-black">Mike Dorney</span> &mdash; <span className="text-muted">CEO
                  and Co-Founder</span></cite>
              </blockquote>
            </div>
          </div>
        </div>
      </div>
    </div>
  </>
)

export default About
