import dynamic from 'next/dynamic'
import { netlifyCmsConfig } from 'lib/netlifyCMS/config'
import { AdminLayout } from 'parts/layout/Admin'

const CMS = dynamic(
  async () => {
    /** @type {import('netlify-cms-app').default} */
    const cms = await import('netlify-cms-app')
    cms.init({ config: netlifyCmsConfig })
  },
  { ssr: false },
)

const Admin = () => {
  return <>
    <div id='nc-root' />
    <CMS />
  </>
}

Admin.title = '後台'
Admin.Layout = AdminLayout

export default Admin
