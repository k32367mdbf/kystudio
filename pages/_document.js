import NextDocument, { Html, Head, Main, NextScript } from 'next/document'
import { nextConfig } from '/config/next'

const { basePath = '' } = nextConfig

const fonts = ['Roboto:wght@400;500;700', 'Noto+Sans+TC:wght@400;500;700']
  .map(f => `family=${f}`)
  .join('&')

class Document extends NextDocument {
  static async getInitialProps({ pathname, renderPage }) {
    return { ...await renderPage(), pathname }
  }

  render() {
    const { pathname = '' } = this.props

    return (
      <Html lang='zh-Hant-TW'>
        <Head>
          <meta charSet="utf-8" />
          <meta name="description" content="宗橋室內裝修" />
          <meta name="keywords" content="室內設計, 室內裝修" />

          {/* Google Fonts */}
          <link
            href={`https://fonts.googleapis.com/css2?${fonts}`}
            rel="stylesheet"
          />

          {!pathname.startsWith('/admin') && <>
            <link rel="stylesheet" href={`${basePath}/fonts/icomoon/style.css`} />
            <link rel="stylesheet" href={`${basePath}/fonts/flaticon/font/flaticon.css`} />
            <link rel="stylesheet" href={`${basePath}/css/bootstrap.min.css`} />
            <link rel="stylesheet" href={`${basePath}/css/animate.min.css`} />
            <link rel="stylesheet" href={`${basePath}/css/jquery.fancybox.min.css`} />
            <link rel="stylesheet" href={`${basePath}/css/owl.carousel.min.css`} />
            <link rel="stylesheet" href={`${basePath}/css/owl.theme.default.min.css`} />
            <link rel="stylesheet" href={`${basePath}/css/aos.css`} />
            <link rel="stylesheet" href={`${basePath}/css/component.css`} />
            <link rel="stylesheet" href={`${basePath}/css/style.css`} />
            <link rel="stylesheet" href={`${basePath}/css/custom.css`} />

            <script src={`${basePath}/js/modernizr.custom.js`} />
          </>}
        </Head>

        {!pathname.startsWith('/admin') ? (
          <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
            <Main />
            <script src={`${basePath}/js/jquery-3.3.1.min.js`} />
            <script src={`${basePath}/js/popper.min.js`} />
            <script src={`${basePath}/js/bootstrap.min.js`} />
            <script src={`${basePath}/js/owl.carousel.min.js`} />
            <script src={`${basePath}/js/jquery.sticky.js`} />
            <script src={`${basePath}/js/jquery.waypoints.min.js`} />
            <script src={`${basePath}/js/jquery.animateNumber.min.js`} />
            <script src={`${basePath}/js/jquery.fancybox.min.js`} />
            <script src={`${basePath}/js/jquery.easing.1.3.js`} />
            <script src={`${basePath}/js/aos.js`} />
            <script src={`${basePath}/js/masonry.pkgd.min.js`} />
            <script src={`${basePath}/js/imagesloaded.js`} />
            <script src={`${basePath}/js/classie.js`} />
            <script src={`${basePath}/js/AnimOnScroll.js`} />
            <NextScript />
          </body>
        ) : (
          <body>
            <Main />
            <NextScript />
          </body>
        )}
      </Html>
    )
  }
}

export default Document
