import { relative } from 'path'
import { root, build } from '/config/path'

const { env } = process

/** @type {import('next').NextConfig} */
export const nextConfig = {
  distDir: relative(root, build),
  basePath: env.SUBDIRECTORY,
  trailingSlash: true,
  typescript: {
    ignoreBuildErrors: true,
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
  env: {
    NEXT_TELEMETRY_DISABLED: true,
  },
}

export default nextConfig
