import { resolve } from 'path'

export const root = process.cwd()

export const build = resolve(root, 'build')
export const dist = resolve(root, 'build/dist')
export const cache = resolve(root, 'build/cache')
export const cmsAssets = resolve(root, 'public/data')
export const cmsContent = resolve(root, 'public/content')

export default root
