/* ***********************************************************************************************
 *   Default environment variables
 *
 * - Do not edit this file directly.
 * - Instead, make a copy and rename it to "env.js" and custom your environment variables there.
 *********************************************************************************************** */

export const env = {
  PORT: 9527,
  SUBDIRECTORY: '',
}

export const expose = {
  /** @type {import('netlify-cms-core').CmsBackendType} */
  NETLIFY_CMS_BACKEND_NAME: 'git-gateway',
  NETLIFY_CMS_BACKEND_REPO: '',
  NETLIFY_CMS_BACKEND_BRANCH: '',
  NETLIFY_CMS_BACKEND_APP_ID: '',
  /** @type {import('netlify-cms-core').CmsBackend['auth_type']} */
  NETLIFY_CMS_BACKEND_AUTH_TYPE: '',
}

export default env
